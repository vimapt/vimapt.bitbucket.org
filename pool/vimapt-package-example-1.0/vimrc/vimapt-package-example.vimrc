" this is a vimrc file for example package of vimapt
" this file will contain the package's config option and keymap.effectively,
" everyone should put his or her's config option and keymap here
" if possible, put all the config option and keymap here and commit it well, so
" everyone kown echo line is what for.

" keymap for vimapt_package_example 
let g:vimapt_package_example_keymap = "<F3>"
